# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2021 Volker Krause <vkrause@kde.org>

import info

class subinfo(info.infoclass):
    def setTargets(self):
        self.description = "KDE OSM Indoor Map"
        self.versionInfo.setDefaultValues(gitUrl = "https://invent.kde.org/libraries/kosmindoormap.git")

    def setDependencies(self):
        self.runtimeDependencies["virtual/base"] = None
        self.buildDependencies["kde/frameworks/extra-cmake-modules"] = None
        self.runtimeDependencies["libs/qt5/qtbase"] = None
        self.runtimeDependencies["libs/qt5/qtdeclarative"] = None
        self.runtimeDependencies["kde/libs/kopeninghours"] = None
        self.runtimeDependencies["kde/libs/kpublictransport"] = None


from Package.CMakePackageBase import *

class Package(CMakePackageBase):
    def __init__(self):
        CMakePackageBase.__init__(self)
        # there is a standalone APK in here, but that's not built by default
        self.androidApkTargets = None
